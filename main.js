let myJson = localStorage.filters || "{}",
	myFilters = JSON.parse(myJson),
	filters = {
	"youtube.com": "yewtu.be",
	"youtu.be": "redirect.invidious.io",
	"www.youtube.com": "yewtu.be",
	"m.youtube.com": myFilters["youtube.com"] || "yewtu.be",
	"twitter.com": "nitter.net",
	"www.twitter.com": "nitter.net",
	"mobile.twitter.com": myFilters["twitter.com"] || "nitter.net",
	"reddit.com": "libredd.it",
	"www.reddit.com": "libredd.it",
	"amp.reddit.com": myFilters["reddit.com"] || "libredd.it"
}
function cancel(arg) {
	const host = arg.url.split("/")[2],
		  rehost = myFilters[host] || filters[host];

	if (rehost) {
		const re = arg.url.replace(host,rehost)
		return {
			redirectUrl: re
		}
	}
}
browser.runtime.onMessage.addListener(function(data){
	localStorage.filters = JSON.stringify(data);
	myFilters = data;
	console.log()
})
browser.webRequest.onBeforeRequest.addListener(cancel,{
	urls: ["<all_urls>"]
},['blocking'])