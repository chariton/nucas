# Nucas

![Mozilla Add-on](https://img.shields.io/amo/v/nucas?logo=firefox&style=for-the-badge)
![Mozilla Add-on](https://img.shields.io/amo/stars/nucas?logo=firefox&style=for-the-badge)

A Simple and Minimal redirector for Reddit, Twitter and YouTube to their privacy respecting counterparts.


## Current filters

 - YouTube -> 
    - [Invidious (yewtu.be)](https://yewtu.be)
    - [Piped (kavin.rocks)](https://piped.kavin.rocks)
    - [Invidious Redirector](https://redirect.invidious.io)

 - YouTube-Shortlinks (youtu.be) ->
    - [Invidious redirector(invidious.io)](https://redirect.invidious.io)
    - [Invidious (yewtu.be)](https://yewtu.be)
    - [Piped (kavin.rocks)](https://piped.kavin.rocks)

 - Twitter ->
    - [Nitter (nitter.net)](https://nitter.net)
    - [Nitter (pussthecat.org)](https://nitter.pussthecat.org)

 - Reddit ->
    - [Libreddit (libredd.it)](https://libredd.it)
    - [Libreddit (r.nf)](https://r.nf)
    - [Teddit (teddit.net)](https://teddit.net)


## Custom filters

1. Modify the `filter` javascript object.
2. Build!

## Downloading

[![Mozilla Add-on Nucas](https://extensionworkshop.com/assets/img/documentation/publish/get-the-addon-178x60px.dad84b42.png)](https://addons.mozilla.org/en-US/firefox/addon/nucas/)

### Firefox

*All releases are signed by mozilla, you don't need Firefox Developers/Nightly/ESR for it to work.*

1. Go to the [releases tab](https://codeberg.org/chariton/nucas/releases).
2. Get the `nucas-X.X.X-fx.xpi` package from the latest release.
3. Click on `Add` Firefox shows a popup.

*If it forces you to download, just Drag and Drop the file to about:addons*

***From source***
1. Download the source code from the releases tab.
2. Go to about:debugging > This Firefox > and click on temporary addon ( If you want it temporarly ).

### Chromium

1. Download the latest `nucas-X.X.X-cr.crx` package from the releases tab.
2. Go to chrome://extentions and turn on developer mode.
3. Drag and Drop the `.crx` file.

**From Source**
1. Download the source code from the releases tab.
2. Replace `browser` with `chrome` in the `.js` files.
3. Turn on developer mode in chrome://extentions and click on load unpacked extention.

## Donations

[ XMR ] `43zag6nP2B1NpcqL2fh8DY6EJhihmCEZC1VHj1CAUWabh1nZu7CQBH6Ui29WNnxTYzQW9CAgre4dBWKjakJxKmF93LNEocv`